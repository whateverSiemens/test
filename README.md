Mail de la Horatiu Condrea



Salut,
 
Joia asta ne vedem la Kronwell, va rog folositi intrarea de pe Bulevardul Gării. Odata ce intrati ar trebui sa vedeti semne catre sala unde avem cursul (semne = sigla Siemens). Daca nu va descurcati, baietii de la receptie o sa va ajute.
 
Daca veniti cu masina, se poate parca in spatele benzinariei OMV, sau se poate lasa masina la sediul firmei si veniti pe jos.
 
Daca aveti alte intrebari nu ezitati sa imi scrieti un mail.
 
Fiecare team leader sa imi confirme primirea acestui mail.